/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin;

import aplikasi.inventaris.KoneksiDatabaseFile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Dzikri Azzakah
 */
public class CRUD_Admin {
    Connection con;
    Statement stat;
    ResultSet rs;
    String sql;
    String id_petugas;
    public void insert_data_pegawai(String nama, String NIP, String alamat, String username, String password) throws SQLException{
         
         KoneksiDatabaseFile databaseConnection = new KoneksiDatabaseFile();
         con = databaseConnection.getKoneksi();
        Statement statement = con.createStatement();
        try {
            String queryinsert ="INSERT INTO pegawai (nama_pegawai,nip,alamat,username,password)"
                    + "VALUES ("+"'"+nama+"'"+","+"'"+NIP+"'"+","+"'"+alamat+"'"+","+"'"+username+"'"+","+"'"+password+"'"+")";
            System.out.println(queryinsert);
            int resultinsert = statement.executeUpdate(queryinsert);
            if(resultinsert == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        } catch(Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan pada Query");
        }
    }
    public void insert_data_barang(String nama, String kondisi, String jumlah, String jenis, String ruang, String kode_inventaris,String keterangan, String id_petugas) throws SQLException{
         KoneksiDatabaseFile databaseConnection = new KoneksiDatabaseFile();
         con = databaseConnection.getKoneksi();
        Statement statement = con.createStatement();
        try {
             Date date = new Date();  
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");  
            String strDate= formatter.format(date);  
    
            
            String queryinsert ="INSERT INTO inventaris (nama,kondisi,jumlah,id_jenis,id_ruang,kode_inventaris,id_petugas,tanggal_register,keterangan)"
                    + "VALUES ("+"'"+nama+"'"+","+"'"+kondisi+"'"+","+"'"+jumlah+"'"+","+"'"+jenis+"'"+","+"'"+ruang+"'"+","+"'"+kode_inventaris+"'"+","+"'"+id_petugas+"'"+","+"'"+strDate+"'"+","+"'"+keterangan+"'"+")";
            System.out.println(queryinsert);
            int resultinsert = statement.executeUpdate(queryinsert);
            if(resultinsert == 1) {
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Ditambahkan");
            }
        } catch(Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan pada Query");
        }
    }
}
