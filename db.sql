/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.35-MariaDB : Database - db_simulasi_ujikom
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_simulasi_ujikom` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_simulasi_ujikom`;

/*Table structure for table `detail_peminjaman` */

DROP TABLE IF EXISTS `detail_peminjaman`;

CREATE TABLE `detail_peminjaman` (
  `id_detail_pinjam` bigint(18) NOT NULL AUTO_INCREMENT,
  `id_inventaris` bigint(18) NOT NULL,
  `jumlah` int(2) NOT NULL,
  `id_peminjaman` bigint(18) NOT NULL,
  `nomor_induk` bigint(18) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `FK_id_peminjaman` (`id_peminjaman`),
  KEY `FK_id_inventaris` (`id_inventaris`),
  KEY `fk_nomor_induk` (`nomor_induk`),
  CONSTRAINT `FK_id_inventaris` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`),
  CONSTRAINT `FK_id_peminjaman` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`),
  CONSTRAINT `fk_nomor_induk` FOREIGN KEY (`nomor_induk`) REFERENCES `warga_sekolah` (`nomor_induk`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `detail_peminjaman` */

insert  into `detail_peminjaman`(`id_detail_pinjam`,`id_inventaris`,`jumlah`,`id_peminjaman`,`nomor_induk`) values 
(1,1,1,1,1718117156);

/*Table structure for table `inventaris` */

DROP TABLE IF EXISTS `inventaris`;

CREATE TABLE `inventaris` (
  `id_inventaris` bigint(18) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  `kondisi` varchar(10) NOT NULL,
  `keterangan` varchar(30) DEFAULT NULL,
  `jumlah` int(2) NOT NULL,
  `id_jenis` bigint(18) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` bigint(18) NOT NULL,
  `kode_inventaris` varchar(20) NOT NULL,
  `id_petugas` bigint(18) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `FK_id_jenis` (`id_jenis`),
  KEY `FK_id_ruang` (`id_ruang`),
  KEY `FK_id_petugas` (`id_petugas`),
  CONSTRAINT `FK_id_jenis` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  CONSTRAINT `FK_id_petugas` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`),
  CONSTRAINT `FK_id_ruang` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `inventaris` */

insert  into `inventaris`(`id_inventaris`,`nama`,`kondisi`,`keterangan`,`jumlah`,`id_jenis`,`tanggal_register`,`id_ruang`,`kode_inventaris`,`id_petugas`) values 
(1,'Proyektor Infocus','Layak','Proyektor Infocus',2,1,'2019-09-01',1,'PROY-Inf',2);

/*Table structure for table `jenis` */

DROP TABLE IF EXISTS `jenis`;

CREATE TABLE `jenis` (
  `id_jenis` bigint(18) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(12) NOT NULL,
  `kode_jenis` bigint(18) NOT NULL,
  `keterangan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `jenis` */

insert  into `jenis`(`id_jenis`,`nama_jenis`,`kode_jenis`,`keterangan`) values 
(1,'Elektronik',0,''),
(2,'ATK',0,'Alat Tulis Kantor');

/*Table structure for table `level` */

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `id_level` int(2) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(10) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `level` */

insert  into `level`(`id_level`,`nama_level`) values 
(1,'superadmin'),
(2,'admin');

/*Table structure for table `pegawai` */

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `id_pegawai` bigint(18) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(20) NOT NULL,
  `nip` bigint(18) NOT NULL,
  `alamat` varchar(70) NOT NULL,
  `password` varchar(16) NOT NULL,
  `username` varchar(16) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pegawai` */

insert  into `pegawai`(`id_pegawai`,`nama_pegawai`,`nip`,`alamat`,`password`,`username`) values 
(1,'Asep Sumanto',198609262015051001,'Jl. Kliningan No.6','asep123','asep'),
(2,'dzikri',1718117156,'sukarma','dazzakah','dazzakah');

/*Table structure for table `peminjaman` */

DROP TABLE IF EXISTS `peminjaman`;

CREATE TABLE `peminjaman` (
  `id_peminjaman` bigint(18) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `status_peminjaman` varchar(12) DEFAULT NULL,
  `id_pegawai` bigint(18) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `FK_id_pegawai` (`id_pegawai`),
  CONSTRAINT `FK_id_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `peminjaman` */

insert  into `peminjaman`(`id_peminjaman`,`tanggal_pinjam`,`tanggal_kembali`,`status_peminjaman`,`id_pegawai`) values 
(1,'2019-09-15','2019-09-15','Dikembalikan',2);

/*Table structure for table `petugas` */

DROP TABLE IF EXISTS `petugas`;

CREATE TABLE `petugas` (
  `id_petugas` bigint(18) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(16) NOT NULL,
  `nama_petugas` varchar(25) NOT NULL,
  `id_level` int(2) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `FK_id_level` (`id_level`),
  CONSTRAINT `FK_id_level` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `petugas` */

insert  into `petugas`(`id_petugas`,`username`,`password`,`nama_petugas`,`id_level`) values 
(1,'maman','maman','Maman Abdurahman',1),
(2,'dadang','dadang','Dadang Sumarni',1),
(3,'asep','aserp','Asep Ujang',1);

/*Table structure for table `ruang` */

DROP TABLE IF EXISTS `ruang`;

CREATE TABLE `ruang` (
  `id_ruang` bigint(18) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(20) NOT NULL,
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ruang` */

insert  into `ruang`(`id_ruang`,`nama_ruang`,`kode_ruang`,`keterangan`) values 
(1,'Lab RPL 1','A-2.6','Laboratorium RPL');

/*Table structure for table `warga_sekolah` */

DROP TABLE IF EXISTS `warga_sekolah`;

CREATE TABLE `warga_sekolah` (
  `nomor_induk` bigint(18) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `status` varchar(15) NOT NULL,
  `password` varchar(16) NOT NULL,
  PRIMARY KEY (`nomor_induk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `warga_sekolah` */

insert  into `warga_sekolah`(`nomor_induk`,`nama`,`status`,`password`) values 
(1718117156,'Dzikri Azzakah Asshiam','Siswa','123456789'),
(1718117157,'Elang Fajar Buana','Siswa','123456789');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
